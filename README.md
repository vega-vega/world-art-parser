# README #

Тестовое задание для парса страницы Word-art.

### Описание работы ###

Для запуска парса необходимо перейти по странице /parse/
Имеет два Get параметра start и end, в которые нужно указать год для парса.

Имеет пользовательский интерфейс для вывода данных по пути /main/

### БД ###

Структура таблицы:

--
-- Структура таблицы `cinema`
--

CREATE TABLE `cinema` (
  `id` int(11) NOT NULL,
  `cinema_group` int(2) DEFAULT 1,
  `position` int(10) DEFAULT NULL,
  `calculated_score` int(10) NOT NULL,
  `vote` int(10) DEFAULT NULL,
  `average_score` float DEFAULT NULL,
  `title` varchar(64) NOT NULL,
  `year` varchar(4) NOT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cinema`
--
ALTER TABLE `cinema`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cinema`
--
ALTER TABLE `cinema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
