var group = 1;
var sort = 0;
var order = true;

function changeGroup(newValue) {
    group = newValue;
    console.log(group);
}

function changeSort(newSort) {
    if (sort === newSort) {
        order = !order;
    } else {
        order = true;
    }

    sort = newSort;
}

function sendRequest() {
    var data2 = {
        "year":$("#year :selected").val(),
        "group":group,
        "sort":sort,
        "order":order
    };

    $.ajax({
        url: "./data/",
        type: "GET",
        data: data2,
        success: function (data) {
            showResponse(data)
        },
        dataType: "json"
    });
}

function showResponse(data) {
    var response = "";
    $('#modal-block').html("");

    for (var key in data) {
        response += "<div class='row justify-content-center' data-toggle='modal' data-target='#modal_" + key + "'>"
            + "<div class='col-lg-6'>"
            + "<div class='card'>"
            + "<div class='row justify-content-center'>"
            + "<div class='col-lg-6'>"
            + "<img class='card-img-top card-img-size' src='http://www.world-art.ru/cinema/" + data[key]['image'] + "' alt='Card image cap'>"
            + "</div>"
            + "<div class='col-lg-6'>"
            + "<h5 class='card-title'>" + data[key]['title'] + "</h5>"
            + "<p class='card-text'>Позиция: " + data[key]['position'] + "</p>"
            + "<p class='card-text'>Голосов: " + data[key]['vote'] + "</p>"
            + "<p class='card-text'>Средний бал: " + data[key]['average_score'] + "/10</p>"
            + "<p class='card-text'>Расчетный бал: " + data[key]['calculated_score'] + "</p>"
            + "</div>"
            + "</div>"
            + "</div>"
            + "</div>"
            + "</div>";

        createModel(key, data);
    }

    $("#response").html(response);
}

function createModel(key, data) {
    var description = data[key]['description'] == '' ? 'Нет описания' : data[key]['description'];
    var modal = "<div class='modal' id='modal_" + key + "'>"
    + "<div class='modal-dialog'>"
    + "<div class='modal-content'>"
    + "<div class='modal-header'>"
    + "<h4 class='modal-title'>" + data[key]['title'] + "</h4>"
    + "<button type='button' class='close' data-dismiss='modal'>&times;</button>"
    + "</div>"
    + "<div class='modal-body'>"
    + "<div class='row justify-content-center'>"
    + "<div class='col-lg-6'>"
    + "<img class='card-img-top card-img-size' src='http://www.world-art.ru/cinema/" + data[key]['image'] + "' alt='Card image cap'>"
    + "</div>"
    + "<div class='col-lg-6'>"
    + "<p class='card-text'>Позиция: " + data[key]['position'] + "</p>"
    + "<p class='card-text'>Голосов: " + data[key]['vote'] + "</p>"
    + "<p class='card-text'>Средний бал: " + data[key]['average_score'] + "/10</p>"
    + "<p class='card-text'>Расчетный бал: " + data[key]['calculated_score'] + "</p>"
    + "</div>"
    + "</div>"
    + "<div class='row'>"
    + "<div class='col-lg-12'>"
    + "<h4 class='card-text'>Описание: </h4>"
    + "<p class='card-text'>" + description + "</p>"
    + "</div>"
    + "</div>"
    + "</div>"
    + "<div class='modal-footer'>"
    + "<button type='button' class='btn btn-danger' data-dismiss='modal'>закрыть</button>"
    + "</div>"
    + "</div>"
    + "</div>"
    + "</div>"

    $('#modal-block').append(modal);
}

$(document).ready(function(){
    $(".update-group").click(function(){
        sendRequest();
    });

    $(".update-year").change(function(){
        sendRequest();
    });

    $(".update-sort").click(function(){
        sendRequest();
    });
});