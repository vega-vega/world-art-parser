<?php

use worldArtParser\Manager\CinemaDBManager;
use worldArtParser\Manager\CinemaParseManager;

?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./public/css/bootstrap.min.css" id="bootstrap-css">
    <link href="./public/css/sticky-footer-navbar.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="chrome-extension://onbkopaoemachfglhlpomhbpofepfpom/styles.css">

    <script src="./public/js/jquery.min.js"></script>
    <script src="./public/js/bootstrap.js"></script>
    <script src="./public/js/main.js"></script>

    <title>Cinema is my life</title>
</head>
<body>
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">Вывод топ 10</a>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item nav-padd">
                    <a class="nav-link update-group" href="#" onclick="changeGroup(<?= CinemaParseManager::GROUP_DATA['cinema'] ?>)">Кино</a>
                </li>
                <li class="nav-item nav-padd">
                    <a class="nav-link update-group" href="#" onclick="changeGroup(<?= CinemaParseManager::GROUP_DATA['rus'] ?>)">Российские сериалы</a>
                </li>
                <li class="nav-item nav-padd">
                    <a class="nav-link update-group" href="#" onclick="changeGroup(<?= CinemaParseManager::GROUP_DATA['jp'] ?>)">Япоские дорамы</a>
                </li>
                <li class="nav-item nav-padd">
                    <a class="nav-link update-group" href="#" onclick="changeGroup(<?= CinemaParseManager::GROUP_DATA['kr'] ?>)">Корейские дорамы</a>
                </li>
                <li class="nav-item nav-padd">
                    <a class="nav-link update-group" href="#" onclick="changeGroup(<?= CinemaParseManager::GROUP_DATA['other'] ?>)">Западные сериалы</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0">
                <h1 class="navbar-brand">Год:</h1>
                <select class="form-control update-year" id="year">
                    <?php
                        for ($i = date("Y"); $i >= 1894; $i--) {
                            echo "<option value='$i'>$i</option>";
                        }
                    ?>
                </select>
            </form>
        </div>
    </nav>

    <div class="container">
        <h4 class="text-center">Сортировать по:</h4>
        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link update-sort" href="#" onclick="changeSort(<?= CinemaDBManager::DB_SORT['title'] ?>)">Названию</a>
            </li>
            <li class="nav-item">
                <a class="nav-link update-sort" href="#" onclick="changeSort(<?= CinemaDBManager::DB_SORT['position'] ?>)">Позиции</a>
            </li>
            <li class="nav-item">
                <a class="nav-link update-sort" href="#" onclick="changeSort(<?= CinemaDBManager::DB_SORT['averageScore'] ?>)">Рейтингу</a>
            </li>
            <li class="nav-item">
                <a class="nav-link update-sort" href="#" onclick="changeSort(<?= CinemaDBManager::DB_SORT['vote'] ?>)">Голосам</a>
            </li>
            <li class="nav-item">
                <a class="nav-link update-sort" href="#" onclick="changeSort(<?= CinemaDBManager::DB_SORT['calculatedScore'] ?>)">Рассчетный балл</a>
            </li>
        </ul>
    </div>

    <div class="container" id="response">
        <h1 class="text-center">Начните поиск</h1>
    </div>
    <div id="modal-block"/>

    <footer class="footer fixed-bottom">
        <p>Git: <a href="https://bitbucket.org/vega-vega/public-task/">@vega-vaga</a></p>
    </footer>
</body>