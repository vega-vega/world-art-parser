<?php

namespace WorldArtParser;

use WorldArtParser\Manager\CinemaDBCache;
use WorldArtParser\Manager\CinemaParseManager;
use WorldArtParser\Manager\CinemaDBManager;

class Controller
{

    public function homeIndex()
    {
        include 'public/page/main_page.php';
    }

    public function parseIndex()
    {
        $parse = new CinemaParseManager();
        $db = new CinemaDBManager();

        if (!isset($_GET["start"])) {
            throw new \Exception("Wrong parameters");
        }
        $start = intval($_GET["start"]);

        if ((isset($_GET["end"]) && !is_int($_GET["end"])) || !isset($_GET["end"])) {
            $end = null;
        } else {
            $end = $_GET["end"];
        }

        $values = $parse->startParse($start, $end);
        $db->addCinemaArray($values);

        echo "ok";
    }

    public function dataIndex()
    {
        $manager = new CinemaDBCache();
        header('Content-type: application/json');
        echo json_encode($manager->getCinemaArr($_GET["group"], $_GET["year"], $_GET["sort"], $_GET["order"]));
    }
}