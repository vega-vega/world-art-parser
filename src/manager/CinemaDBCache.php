<?php

namespace WorldArtParser\Manager;

use Memcache;

class CinemaDBCache
{
    /**
     * Считываем значение из базы.
     *
     * @param $group
     * @param $year
     * @param $sort
     * @param $order
     * @return array|false|string
     */
    public function getCinemaArr($group, $year, $sort, $order)
    {
        $keyName = $group . $year . $sort . $order;
        $db = new CinemaDBManager();

        $memcache = new Memcache;
        $memcache->connect('127.0.0.1', 11211) or die("Could not connect");

        $value = @$memcache->get($keyName);
        if(empty($value)) {
            $value = $db->getCinemaArr($_GET["group"], $_GET["year"], $_GET["sort"], $_GET["order"]);
            $memcache->set($keyName, $value, false, 86400); // храним один день.
        }

        $memcache->close();

        return $value;
    }
}