<?php

namespace WorldArtParser\Manager;

use DOMDocument;
use DOMXPath;
use WorldArtParser\Model\CinemaModel;

class CinemaParseManager
{
    const GROUP_DATA = ["cinema" => 1, "rus" => 2, "jp" => 3, "kr" => 4, "other" => 5];

    /**
     * Парс сайт по годам.
     * Возвращает массив обхектов для дальнейшей работы.
     *
     * @param int $startDate
     * @param int $endDate
     * @return array[]
     */
    public function startParse($startDate, $endDate = null)
    {
        $endDate = $endDate == null ? $startDate : $endDate;
        $cinemaArr = [];
        for ($currentYear = $startDate; $currentYear <= $endDate; $currentYear++) {
            $cinema = "http://www.world-art.ru/cinema/list.php?public_type=1&public_year=$currentYear&status=3&sort=1";
            $cinemaArr = array_merge($cinemaArr, $this->start($cinema, $currentYear, self::GROUP_DATA['cinema']));

            $rus = "http://www.world-art.ru/cinema/list.php?public_type=2&public_country=1&public_year=$currentYear&status=3&sort=1";
            $cinemaArr = array_merge($cinemaArr, $this->start($rus, $currentYear, self::GROUP_DATA['rus']));

            $jp = "http://www.world-art.ru/cinema/list.php?public_type=2&public_country=2&public_year=$currentYear&status=3&sort=1";
            $cinemaArr = array_merge($cinemaArr, $this->start($jp, $currentYear, self::GROUP_DATA['jp']));

            $kr = "http://www.world-art.ru/cinema/list.php?public_type=2&public_country=46&public_year=$currentYear&status=3&sort=1";
            $cinemaArr = array_merge($cinemaArr, $this->start($kr, $currentYear, self::GROUP_DATA['kr']));

            $countryArr = [3,5,7,15,16,17,225,22,23,25,26,28,36,37,38,39,49,50,104,105,110,122,123,133,138,157,158,162,179,180,199,203,204,206,209,212,214,215,220];
            foreach ($countryArr as $country) {
                $other = "http://www.world-art.ru/cinema/list.php?public_type=2&public_country=$country&public_year=$currentYear&status=3&sort=1";
                $value = $this->start($other, $currentYear, self::GROUP_DATA['other']);
                $cinemaArr = array_merge($cinemaArr, $value);
            }
        }

        return $cinemaArr;
    }

    private function start($url, $year, $group)
    {
        $arr = $this->parseUrlFromCategory($url);
        return $this->parsePageForValue($arr, $year, $group);
    }

    /**
     * Находит все ссылки по фильту, для дальшнейшего парса.
     *
     * @param $searchUrl
     * @return array
     */
    private function parseUrlFromCategory($searchUrl)
    {
        $agent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
        ini_set('user_agent', $agent);

        $dom = new DOMDocument();
        @$dom->loadHTMLFile($searchUrl);
        $nodes = $this->getNodes($dom);

        $count = 0;
        foreach ($nodes as $node) {
            if (stripos($node->textContent, 'Результат выборки') !== false) {
                $count = preg_replace("/[^0-9]/", '', $node->childNodes->item(0)->childNodes->item(0)->textContent);
                break;
            }
        }

        $urlArr = $this->parsePageUrl($nodes);
        if ($count != count($urlArr)) {
            $searchUrl .= "&limit_1=";
            $step = 1;
            $maxStep = round($count / 50);

            while ($maxStep >= $step) {
                @$dom->loadHTMLFile($searchUrl . ($step++ * 50));
                $nodes = $this->getNodes($dom);
                $urlArr = array_merge($urlArr, $this->parsePageUrl($nodes));
            }
        }

        return $urlArr;
    }

    /**
     * Возвращает нужный кусок страницы для дальнейшей обработки.
     *
     * @param DOMDocument $dom
     * @return \DOMNodeList|false
     */
    private function getNodes(DOMDocument $dom)
    {
        $xpath = new DOMXPath($dom);
        return  $xpath->query("//table[@bgcolor='#ffffff']/tr/td/table/tr/td[@valign='top']/table");
    }

    /**
     * Находит все ссылки на станице.
     * Входящие данные - страницца поиска по категориям.
     * На выход - массив ссылок на страницы.
     *
     * @param \DOMNodeList $nodes
     * @return array['string']
     */
    private function parsePageUrl(\DOMNodeList $nodes)
    {
        $urlArr = [];

        $isNext = false;
        foreach ($nodes as $node) {
            $bgcolor = $node->attributes->getNamedItem("bgcolor");
            if ($bgcolor != NULL && $bgcolor->textContent == '#eaeaea') {
                $isNext = true;
                continue;
            }

            if ($isNext) {
                $isNext = false;
                $url = "http://www.world-art.ru/cinema/" . $node->childNodes->item(0)->childNodes->item(2)->childNodes->item(2)->attributes->getNamedItem('href')->textContent;
                array_push($urlArr, $url);
            }
        }

        return $urlArr;
    }

    /**
     * Парсит страницы на данные который в будущем занесем в базу.
     *
     * @param $urlArr
     * @param $year
     * @param $group
     * @return array
     */
    private function parsePageForValue($urlArr, $year, $group)
    {
        $agent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
        ini_set('user_agent', $agent);

        $dom = new DOMDocument();
        $cinemaArr = [];
        foreach ($urlArr as $url) {
            $cinema = new CinemaModel();

            @$dom->loadHTMLFile($url);

            $xpath = new DOMXPath($dom);
            $nodes = $xpath->query("//table[@bgcolor='#ebebeb']/tr/td[@height='470']/table");

            $imgSrc = $this->getImage($nodes);
            $data = $nodes->item(1)->firstChild->childNodes;
            $data = $data->item($data->length - 1)->childNodes;
            $count = $data->length;

            $title = $data->item(1)->textContent;
            $averageScore = $this->getAverageScore($data, $count);
            $vote = $this->getVote($data, $count);
            $position = $this->getPosition($data, $count);
            $description = $this->getDescription($nodes);

            $cinema->setGroup($group);
            $cinema->setImage($imgSrc);
            $cinema->setAverageScore($averageScore);
            $cinema->setCalculatedScore($averageScore);
            $cinema->setDescription($description);
            $cinema->setPosition($position);
            $cinema->setTitle($title);
            $cinema->setVote($vote);
            $cinema->setYear($year);

            $cinemaArr[] = $cinema;
        }

        return $cinemaArr;
    }

    /**
     * @param $nodes
     * @return string
     */
    private function getImage($nodes)
    {
        $value = $nodes->item(1)->firstChild->firstChild->firstChild->firstChild
            ->firstChild->firstChild->firstChild->firstChild->attributes->getNamedItem("src");

        if (is_object($value)) {
            return $value->textContent;
        }

        return "";
    }

    /**
     * @param $data
     * @param $count
     * @return float
     */
    private function getAverageScore($data, $count)
    {
        if ($count >= 3) {
            $value = $data->item($count - 4)->firstChild;
            if (is_object($value)) {
                $value = $value->childNodes->item(2);
                if (is_object($value)) {
                    $value = explode(' ', $value->textContent);
                    return $value[0];
                }
            }
        }

        return "";
    }

    /**
     * @param $data
     * @param $count
     * @return string
     */
    private function getVote($data, $count)
    {
        if ($count >= 3) {
            $value = $data->item($count - 3)->firstChild->childNodes->item(2);

            if (is_object($value)) {
                $value = explode(' ', $value->textContent);
                return $value[0];
            }
        }

        return "";
    }

    /**
     * @param $data
     * @param $count
     * @return string
     */
    private function getPosition($data, $count)
    {
        if ($count >= 3) {
            $value = $data->item($count - 2)->firstChild->childNodes->item(2);

            if (is_object($value)) {
                $value = explode(' ', $value->textContent);
                return $value[0];
            }
        }

        return "";
    }

    /**
     * @param $nodes
     * @return string
     */
    private function getDescription($nodes)
    {
        $value = $nodes->item(2);
        if (is_object($value) && $nodes->item(2)->textContent == 'Краткое содержание') {
            return $nodes->item(3)->firstChild->textContent;
        }

        return "";
    }
}
