<?php

namespace WorldArtParser\Manager;

use PDO;
use WorldArtParser\Config;

class CinemaDBManager
{
    const DB_SORT = ['position' => 0, 'vote' => 1, 'averageScore' => 2, 'title' => 3];

    /**
     * Добавляет распарсенные данные в базу.
     *
     * @param $cinemaArr
     */
    public function addCinemaArray($cinemaArr)
    {
        $conn = $this->getConnection();
        $sql = "INSERT into cinema (cinema_group, description, position, vote, average_score, image, calculated_score, title, year) VALUES (:group, :description, :position, :vote, :averageScore, :image, :calculatedScore, :title, :year) ON DUPLICATE KEY UPDATE description = :description, position = :position, vote = :vote, average_score = :averageScore, image = :image, calculated_score = ((:calculatedScore + calculated_score) / 2);";

        foreach ($cinemaArr as $cinema) {
            $stmt = $conn->prepare($sql);

            $stmt->bindParam(':group', $cinema->getGroup(), PDO::PARAM_INT);
            $stmt->bindParam(':description', $cinema->getDescription(), PDO::PARAM_STR);
            $stmt->bindParam(':position', $cinema->getPosition(), PDO::PARAM_INT);
            $stmt->bindParam(':vote', $cinema->getVote(), PDO::PARAM_INT);
            $stmt->bindParam(':averageScore', $cinema->getAverageScore());
            $stmt->bindParam(':image', $cinema->getImage(), PDO::PARAM_STR);
            $stmt->bindParam(':calculatedScore', $cinema->getCalculatedScore(), PDO::PARAM_INT);
            $stmt->bindParam(':title', $cinema->getTitle(), PDO::PARAM_STR);
            $stmt->bindParam(':year', $cinema->getYear(), PDO::PARAM_STR);

            $stmt->execute();
        }

        $conn = null;
    }

    /**
     * Выгружает записи по группе и году.
     *
     * @param $group
     * @param $year
     * @param $sort
     * @param $order
     * @return array
     */
    public function getCinemaArr($group, $year, $sort, $order)
    {
        $conn = $this->getConnection();
        $sql = $this->getSQL($sort, $order);

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":group", $group, PDO::PARAM_INT);
        $stmt->bindParam(":year", $year, PDO::PARAM_STR);
        $stmt->execute();

        $cinemaArr = [];
        foreach ($stmt as $row) {
            $cinemaArr[] = $row;
        }

        $conn = null;

        return $cinemaArr;
    }

    /**
     * @return PDO
     */
    private function getConnection()
    {
        $host = Config::getInstance()->getParam("host");
        $dbName = Config::getInstance()->getParam("dbName");
        $username = Config::getInstance()->getParam("username");
        $password = Config::getInstance()->getParam("password");

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];

        return new PDO("mysql: host=$host; dbname=$dbName", $username, $password, $opt);
    }

    /**
     * Формируем запрос в зависимости от параметров.
     *
     * @param $sort
     * @param $order
     * @return string
     */
    private function getSQL($sort, $order)
    {
        $sql = "SELECT cinema_group, description, position, vote, average_score, image, calculated_score, title, year FROM cinema WHERE cinema_group = :group AND year = :year ";

        switch ($sort) {
            case self::DB_SORT['vote']:
                $sql .= " order by vote";
                break;
            case self::DB_SORT['averageScore']:
                $sql .= " order by average_score";
                break;
            case self::DB_SORT['title']:
                $sql .= " order by title";
                break;
            case self::DB_SORT['calculatedScore']:
                $sql .= " order by calculated_score";
                break;
            default:
                $sql .= " order by position";
        }

        $sql .= $order == 'true' ? "" : " desc";
        $sql .=  " limit 10";

        return $sql;
    }
}