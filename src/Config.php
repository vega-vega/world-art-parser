<?php


namespace WorldArtParser;


class Config
{

    /**
     * @var Config
     */
    private static $instance;

    private $param;

    private function __construct()
    {
        $strJsonFileContents = file_get_contents("config.json");
        $this->param = json_decode($strJsonFileContents, true);
    }

    public static function getInstance()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getParam($name)
    {
        return $this->param[$name];
    }
}