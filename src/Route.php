<?php

namespace WorldArtParser;

require_once 'src/Controller.php';
require_once 'src/model/CinemaModel.php';
require_once 'src/manager/CinemaParseManager.php';
require_once 'src/manager/CinemaDBManager.php';
require_once 'src/manager/CinemaDBCache.php';
require_once 'src/Config.php';

class Route
{
    static function start()
    {
        $controller = new Controller();

        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $path = $routes[1] == 'world-art-parser' ? $routes[2] : $routes[1];

        if ($path == 'parse') {
            $controller->parseIndex();
        } elseif ($path == 'data') {
            $controller->dataIndex();
        } else {
            $controller->homeIndex();
        }
    }
}